import { Component as _Component, ReactNode as _ReactNode } from 'react'

const React: { Component: typeof _Component } = (global as any).React

let root: Root | undefined

export class Root extends React.Component<{ render(): _ReactNode }> {
  public componentWillMount() {
    root = this
  }

  public render() {
    return this.props.render()
  }
}

let updateTimeout: number

export const state = <TState extends { [key: string]: any }>(
  initialState: TState
): TState => {
  let currentState: { [key: string]: any } = { ...(initialState as object) }

  const engine: TState = {} as any

  Object.keys(initialState).forEach(key => {
    Object.defineProperty(engine, key, {
      get() {
        return currentState[key]
      },

      set(value) {
        if (typeof root === 'undefined') {
          throw new Error(
            '<Root> element must be rendered before updating state'
          )
        }

        if (value === currentState[key]) {
          return
        }

        currentState = { ...currentState, [key]: value }
        clearTimeout(updateTimeout)
        updateTimeout = setTimeout(() => {
          if (typeof root === 'undefined') {
            throw new Error(
              '<Root> element must be rendered before updating state'
            )
          }
          root.forceUpdate()
        })
      }
    })
  })

  return engine
}
